#pragma once

#include <ntifs.h>
#include <ntddk.h>
#include <windef.h>
#include <intrin.h>
#include <stdarg.h>
#include <ntstrsafe.h>

#include "imports.hpp"
#include "shithook.hpp"
#include "utils.hpp"

PVOID(__fastcall* MmGetSystemRoutineAddress_orig)(PUNICODE_STRING);
bool(__fastcall* IsProcessVerified_orig)(uintptr_t*, PEPROCESS);
__m128i* (__fastcall* wtfFunc_orig)(__m128i*, uintptr_t, uintptr_t);

LARGE_INTEGER systemtime;
TIME_FIELDS time;
HANDLE pid;
PEPROCESS process;

//__m128i* wtfFunc_hook(__m128i* a1, uintptr_t a2, uintptr_t a3)
//{
//	LogToFile("[+] WhatTheFuck called a1: 0x%llx a2: 0x%llx a3: 0x%llx\n", *a1, a2, a3);
//	return wtfFunc_orig(a1, a2, a3);
//}

bool IsProcessVerified_hook(uintptr_t* VerifiedProcessList, PEPROCESS proc)
{
	if (PsLookupProcessByProcessId((HANDLE)24592, &process) == STATUS_SUCCESS)
	{
		if (process == proc)
			return true;
	}
	return IsProcessVerified_orig(VerifiedProcessList, proc);
}

NTSTATUS ObRegisterCallbacks_hook(POB_CALLBACK_REGISTRATION CallbackRegistration, PVOID* RegistrationHandle)
{
	LogToFile("[+] ObRegisterCallbacks Called\n");
	return STATUS_SUCCESS;
}

void ObUnRegisterCallbacks_hook(PVOID RegistrationHandle)
{
	LogToFile("[+] ObRegisterCallbacks Called\n");
}

PVOID MmGetSystemRoutineAddress_hook(PUNICODE_STRING SystemRoutineName)
{
	LogToFile("[+] MmGetSystemRoutineAddress called %ws\n", SystemRoutineName->Buffer);
	if (wcsstr(SystemRoutineName->Buffer, L"ObRegisterCallbacks"))
	{
		LogToFile("[+] ObRegisterCallbacks Hooked\n");
		return &ObRegisterCallbacks_hook;
	}
	else if (wcsstr(SystemRoutineName->Buffer, L"ObUnRegisterCallbacks"))
	{
		LogToFile("[+] ObUnRegisterCallbacks Hooked\n");
		return &ObUnRegisterCallbacks_hook;
	}

	return MmGetSystemRoutineAddress(SystemRoutineName);
}

void NotifyRoutine(PUNICODE_STRING FullImageName, HANDLE ProcessId, PIMAGE_INFO ImageInfo)
{
	if (!ProcessId && FullImageName && wcsstr(FullImageName->Buffer, L"EQU8_HELPER_19.sys"))
	{
		KeQuerySystemTime(&systemtime);
		RtlTimeToTimeFields(&systemtime, &time);

		LogToFile("============== Log Date: %02d/%02d/%04d at %02d:%02d ==============\n", time.Day, time.Month, time.Year, time.Hour, time.Minute);
		LogToFile("[+] Found driver: %ws\n", FullImageName->Buffer);

		uintptr_t MmGetSystemRoutineAddress_IAT = reinterpret_cast<uintptr_t>(ImageInfo->ImageBase) + 0x2048;
		uintptr_t IsProcessVerified_address = reinterpret_cast<uintptr_t>(ImageInfo->ImageBase) + 0x15A4;

		//disable WP bit
		UINT64 cr0 = __readcr0();
		cr0 &= 0xfffffffffffeffff;
		__writecr0(cr0);

		*(PVOID*)&MmGetSystemRoutineAddress_orig = InterlockedExchangePointer((volatile PVOID*)MmGetSystemRoutineAddress_IAT, &MmGetSystemRoutineAddress_hook);
		LogToFile("[+] Hooked MmGetSystemRoutineAddress\n");

		//re-enable WP bit
		cr0 = __readcr0();
		cr0 |= 0x10000;
		__writecr0(cr0);

		if (!TrampolineHook(&IsProcessVerified_hook, reinterpret_cast<PVOID>(IsProcessVerified_address), (PVOID*)&IsProcessVerified_orig))
			LogToFile("[!] Failed to setup trampoline hook\n");
	}
}

NTSTATUS DriverEntry()
{
	DbgPrintEx(0, 0, "[+] Driver created\n");
	auto E8Base = GetSystemModuleBase("EQU8_HELPER_19.sys");
	if (E8Base) {
		DbgPrintEx(0, 0, "[+] E8 base: %p\n", E8Base);
		bool* bIsCbActive = reinterpret_cast<bool*>(E8Base + 0x3099);
		DbgPrintEx(0, 0, "[+] bIsCbActive: %d\n", *bIsCbActive);
		*bIsCbActive = false;
		DbgPrintEx(0, 0, "[+] bIsCbActive: %d\n", *bIsCbActive);
		DbgPrintEx(0, 0, "[+] Fin\n");

	}
	else {
		DbgPrintEx(0, 0, "[+] Could not find E8\n");
	} 


    //todo .data ptr shit or zwswapcert to get rid of PG callback fuckery

	//PsSetLoadImageNotifyRoutine(&NotifyRoutine);
	//DbgPrintEx(0, 0, "[+] Created load image notify routine\n");


	return STATUS_SUCCESS;
}